""" Unit test for reportbug.ui.text_ui module """

import unittest

import reportbug.ui.text_ui as ui

test_hierarchy = [
    ('normal', ['#123  had better work', '#234444444  makes funny noise']),
    ('serious', ['#999 listen to your parents', '#1000 please behave']),
    ('minor', ['#2 oh, sorry', '#1  you can do better']),
    ('wishlist', ['#9 looks better now', '#3333333333333 better is good']),
    ('critical', ['  #23  hehe', ' #042 hoho']),
]


class TestUI(unittest.TestCase):

    def test_bugnum(self):
        count, bugs = ui.proc_hierarchy(test_hierarchy)
        self.assertEqual(count, 10)
        self.assertEqual(len(bugs), 10)
        self.assertEqual(bugs, [123, 234444444, 999, 1000, 2, 1, 9, 3333333333333, 23, 42])
